import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
    apiKey: "AIzaSyCDY0gkAZS9AVhNhEbdTlV4LTWvfHlCrjc",
    authDomain: "crud-app-4f2d6.firebaseapp.com",
    projectId: "crud-app-4f2d6",
    storageBucket: "crud-app-4f2d6.appspot.com",
    messagingSenderId: "515149840463",
    appId: "1:515149840463:web:3d35ee3c5be63e9f352a3a",
    measurementId: "G-73WVSS9C7P"
};

const app = initializeApp(firebaseConfig);

export const firestoreDB = getFirestore(app);