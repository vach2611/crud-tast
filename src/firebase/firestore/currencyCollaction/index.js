import { firestoreDB } from "../../config";
import { collection, getDocs, addDoc, updateDoc, deleteDoc, doc } from "firebase/firestore";
import { transformData } from "./utils/transformData.util";
import {CRUD_DATA} from "./constants";

const currencyCollectionRef = collection(firestoreDB, CRUD_DATA);

export const getCurrencies = async () => {
    const data = await getDocs(currencyCollectionRef);
    return transformData(data);
};

export const addCurrency = async (payload) => {
    const {id} = await addDoc(currencyCollectionRef, { ...payload });
    return {...payload, id}
};

export const updateCurrency = async (payload) => {
    const data = doc(firestoreDB, CRUD_DATA, payload.id);
    await updateDoc(data, {...payload});
    return payload
};

export const deleteCurrency = async id => {
    const data = doc(firestoreDB, CRUD_DATA, id);
    await deleteDoc(data);
    return id;
};
