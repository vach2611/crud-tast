export const transformData = data => data.docs.map(doc => ({ ...doc.data(), id: doc.id }));

