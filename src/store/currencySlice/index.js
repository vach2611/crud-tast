import { createSlice, createAsyncThunk, createSelector } from '@reduxjs/toolkit';
import { getCurrencies, addCurrency, deleteCurrency, updateCurrency } from "../../firebase/firestore/currencyCollaction";

export const fetchCurrenciesData = createAsyncThunk(
    'currency/fetchCurrenciesData',
    getCurrencies
)

export const addCurrencyData = createAsyncThunk(
    'currency/addCurrencyData',
    (payload) => addCurrency(payload)
)

export const updateCurrencyData = createAsyncThunk(
    'currency/updateCurrencyData',
    (payload) => updateCurrency(payload)
)

export const deleteCurrencyData = createAsyncThunk(
    'currency/deleteCurrencyData',
    (id) => deleteCurrency(id)
)

export const currencySlice = createSlice({
    name: 'currency',
    initialState: {
        data: []
    },
    extraReducers: {
        [fetchCurrenciesData.fulfilled]: (state, {payload}) => {
            state.data = payload
        },
        [addCurrencyData.fulfilled]: (state, {payload}) => {
            state.data.push(payload);
        },
        [deleteCurrencyData.fulfilled]: (state, {payload}) => {
            state.data = state.data.filter(item => item.id !== payload)
        },
        [updateCurrencyData.fulfilled]: (state, {payload}) => {
            state.data[payload.tableId] = {...payload}
        },
    }
})

export const currencyData = ({currency}) => currency.data;
export const currencyDataSelector = createSelector(currencyData, data => data.map(item => ({...item})))

export default currencySlice.reducer
