export const columns = [
    { title: 'ID', render: rowData => rowData.tableData.id + 1 },
    { title: 'Currency Name', field: 'name', validate: rowData => rowData.name === '' ? 'Name cannot be empty' : '' },
    { title: 'Rate (1$ = X rate)', field: 'rate', type: 'numeric' },
]