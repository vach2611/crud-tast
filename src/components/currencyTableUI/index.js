import {useEffect} from "react";
import MaterialTable from 'material-table';
import {useDispatch, useSelector} from "react-redux";
import {
    fetchCurrenciesData,
    currencyDataSelector,
    updateCurrencyData,
    addCurrencyData,
    deleteCurrencyData
} from "../../store/currencySlice";
import {columns} from "./constants";

export const Table = () => {
    const data = useSelector(currencyDataSelector);
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(fetchCurrenciesData())
    }, [dispatch]);

    const onRowUpdate = async (updatedData, {tableData}) => {
        await dispatch(updateCurrencyData({...updatedData, tableId: tableData.id}))
    }

    const onRowAdd = async (newData) => {
        await dispatch(addCurrencyData(newData))
    }

    const onRowDelete = async ({id}) => {
        await dispatch(deleteCurrencyData(id))
    }

    return (
        <>
            <MaterialTable
                title="Custom Currencies"
                columns={columns}
                data={data}
                options={{
                    actionsColumnIndex: -1,
                    paging: false,
                }}
                editable={{
                    onRowUpdate,
                    onRowAdd,
                    onRowDelete,
                }}
            />
        </>
    )
}