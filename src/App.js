import './App.css';
import {Table} from "./components/currencyTableUI";

function App() {
  return (
    <div className="App">
      <Table/>
    </div>
  );
}

export default App;
